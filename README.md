# Cuitter
A cute rails exercice to simulate the well known website Twitter ! 

## Tests Versions
``$ rails --version``

Rails 5.2.3

``$ ruby -v``

ruby 2.5.5p157 (2019-03-15 revision 67260) [x64-mingw32]

### What is does
* Welcome page because it is important to great the visitor 

![](https://framapic.org/QK82DhvtTVAr/Tz3xgRRZmh7J)

* Allows to sign up with form validation

![](https://framapic.org/AkD2Kd63dJJ0/cJCYOATwr8za)

* Allows to log in using the given user 

![](https://framapic.org/zNdgG5wXEERX/qSXAsGLhF0n2)

* Allows to tweet with selection, order and more

![](https://framapic.org/n7L8FIq5K5zd/Er1uizoI0ije)
 
* Allows basic CRUD operations on these posts 

![](https://framapic.org/ga2ALU2wbSna/1Vs4lz3pq6Og)

