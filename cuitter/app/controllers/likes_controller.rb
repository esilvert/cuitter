class LikesController < ApplicationController

    # -----------------------------
    # New
    # -----------------------------
    def new 
        @like = Like.new()
    end

    # -----------------------------
    # Create
    # -----------------------------
    def create
        @like = Like.new(like_params)

        if @like.save() == false
            render plain: @like.errors.inspect # debug
        else
            redirect_to user_path(params[:user_id])
        end
    end

private
    # -----------------------------
    # Params
    # -----------------------------
    def like_params
        params.permit(:user_id, :post_id)
    end

end
