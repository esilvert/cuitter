class PostsController < ApplicationController
  
  # -----------------------------
  # New
  # -----------------------------
  def new
    @user = User.find(params[:user_id])
    @post = @user.posts.build
  end

  # -----------------------------
  # Create
  # -----------------------------
  def create
    @user = User.find(session[:current_user_id])

    @post = @user.posts.build(post_params)

    if @post.save
      redirect_to user_path(@user)
    else
      render 'users/edit'
    end
  end

  # -----------------------------
  # Edit
  # -----------------------------
  def edit
    @user = User.find(session[:current_user_id])
    @post = Post.find(params[:id])
  end

  # -----------------------------
  # Update
  # -----------------------------
  def update
    @post = Post.find(params[:id])

    if @post.update(post_params)
      redirect_to user_path(session[:current_user_id])
    else
      render 'edit'
    end
  end

  # -----------------------------
  # Show
  # -----------------------------
  def show
    @post = Post.find(params[:id])
  end

  # -----------------------------
  # Index
  # -----------------------------
  def index
  end

  # -----------------------------
  # Destroy
  # -----------------------------
  def destroy
    # render plain: params.inspect
    @post = Post.find(params[:id])
    @post.delete

    redirect_to user_path(params[:user_id])
  end

private
  def post_params
    params.require(:post).permit(:content)
  end
end
