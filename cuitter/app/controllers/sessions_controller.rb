class SessionsController < ApplicationController
    include BCrypt

    # -----------------------------
    #  Log in
    # -----------------------------
    def log_in 
        @user = User.new
    end

    # -----------------------------
    #  Sign up
    # -----------------------------
    def sign_up 
        @user = User.new
    end

    
    # -----------------------------
    #  Create
    # -----------------------------
    def create
        # First - Find if user exists
        @user = User.find_by(nickname: user_params[:nickname])

        if @user.nil? 
            @user = User.new
            @user.errors.add(:base, 'No known user match this nickname and password.')
            render 'log_in'
        else
            sha1_password = Digest::SHA1.hexdigest("#{params[:password]}")
            passwordMatch = BCrypt::Password.new(@user.password) == sha1_password
            
            if passwordMatch
                session[:current_user_id] = @user.id
                session[:current_user] = @user
                redirect_to user_path(@user)
            else
                @user.errors.add(:base, 'No known user match this nickname and password.')
                render 'log_in'
            end
        end
    end

    
    # -----------------------------
    #  Destroy
    # -----------------------------
    def destroy
    end

    private
    def user_params
        params.require(:user).permit(:nickname, :password)
    end
end
