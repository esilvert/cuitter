class UsersController < ApplicationController
    include BCrypt


    # -----------------------------
    # New
    # -----------------------------
    def new 
    end

    # -----------------------------
    # Create
    # -----------------------------
    def create 
        params = user_params_create
        @user = User.new(params.except(:password_double_check))
        
        if params[:password] == params[:password_double_check]
            
            sha1_password = Digest::SHA1.hexdigest("#{params[:password]}")
            @user.password = BCrypt::Password.create(sha1_password).to_s

            if @user.save 
                redirect_to log_in
            else
                render 'sessions/sign_up'
            end
        else
            @user.errors.add(:password, ': The two passwords don\'t match')
            render 'sessions/sign_up'
        end
    end

    # -----------------------------
    # Show
    # -----------------------------
    def show 
        @user = User.find(params[:id])
        @post = Post.new
        @posts = Post.where(:user => params[:id]).order( created_at: :desc )
        @likes = {}
        @posts.each do |post|
            @likesRequest = Like.find_by(post_id: post.id)
            @likes.merge(post.id => @likesRequest) unless @likesRequest.nil?
        end
    end

    # -----------------------------
    #  Index
    # -----------------------------
    def index 
        @users = User.all #todo: user search page
    end

    
    # -----------------------------
    #  Log in
    # -----------------------------
    def log_in
        @user = User.new
    end

    # -----------------------------
    # Edit
    # -----------------------------
    def edit
        @user = User.find(params[:id])
    end

    # -----------------------------
    # Update
    # -----------------------------
    def update 
        @user = User.find(params[:id])
        if @user.update(bio: params[:user][:bio], birthday: params[:user][:birthday])
            redirect_to user_path(@user)
        else
            render 'edit'
        end
    end

    # -----------------------------
    #  Destroy
    # -----------------------------
    def destroy 
        @user = User.find(params[:id])
        @user.destroy 
        redirect_to users_path
    end


private
    # -----------------------------
    # User params - Create
    # -----------------------------
    def user_params_create
        params.require(:user).permit(:nickname, :email, :birthday, :password, :password_double_check)
    end
end