class Post < ApplicationRecord

    belongs_to :user

    # -----------------------------
    # Validates
    # -----------------------------

    # > User
    validates :user, presence: true

    # > Content
    validates :content, length: {minimum: 1, maximum: 150}, presence: true

    # > Origin post
    # validates :origin_post, allow_nil: true #> You need to supply at least one validation
end
