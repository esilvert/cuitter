class User < ApplicationRecord
    has_many :posts


# -----------------------------
# Validates
# -----------------------------

# > Nickname
validates :nickname, length: {minimum: 1, maximum: 25}, presence: true,
            uniqueness: {message: -> (object, data) do "#{data[:value]} is already taken." end}

# > Bio
validates :bio, length: {maximum: 75}, allow_blank: true

# > Email
validates :email, allow_blank: false, presence: true,
            uniqueness: {message: -> (object, data) do "#{data[:value]} is already used." end}

# > Password
validates :password, presence: true, length: {minimum: 5}, on: :create

# -----------------------------
# Validates - Format
# -----------------------------

# > Nickname
validates_format_of :nickname, with: /[A-Za-z]/, on: :create, message: "is invalid (format)"

# > Email
validates_format_of :email, with: /\A\w+@\w+\.\w+\z/, on: :create, message: "is invalid (format)"
end
