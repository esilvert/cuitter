Rails.application.routes.draw do
  # ------------------
  # Sessions 
  # ------------------
  get     'sign_up' => 'sessions#sign_up',      :as => 'sign_up'
  post    'connect' => 'sessions#create',   :as => 'connect'
  get     'log_in'  => 'sessions#log_in',      :as => 'log_in'
  delete  'log_out' => 'sessions#destroy',  :as => 'log_out'

  # ------------------
  # Welcome
  # ------------------
  get     'welcome/index'

  # ------------------
  # Resources
  # ------------------
  resources :users do
    resources :posts
  end

  # > Likes ressources
  resources :likes, only: [:create, :destroy]

  # ------------------
  # Root
  # ------------------
  root    'welcome#index' #temp
end
