class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nickname
      t.text :bio
      t.string :email
      t.datetime :birtday

      t.timestamps
    end
  end
end
