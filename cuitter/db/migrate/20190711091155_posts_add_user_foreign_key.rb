class PostsAddUserForeignKey < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :user_id, :integer, references: :users
  end
end
