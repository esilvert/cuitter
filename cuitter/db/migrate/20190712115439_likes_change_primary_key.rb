class LikesChangePrimaryKey < ActiveRecord::Migration[5.2]
  def change
    execute 'ALTER TABLE likes ADD UNIQUE (user_id, post_id);'
  end
end
 